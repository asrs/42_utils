#!/bin/sh

if [ $# -eq 0 ]
then
	echo "Usage: ./Define.sh [ path/to/header.h ]"
	exit;
fi

echo "#define malloc(x) 0" >> $1
