#!/usr/bin/env python3

import re
import os
import sys

# define COLOR_RED     "\x1b[31m"
# define COLOR_GREEN   "\x1b[32m"
# define COLOR_YELLOW  "\x1b[33m"
# define COLOR_BLUE    "\x1b[34m"
# define COLOR_MAGENTA "\x1b[35m"
# define COLOR_CYAN    "\x1b[36m"
# define COLOR_RESET   "\x1b[0m"

#==============================================================================#
#------------------------------------------------------------------------------#
# Utils function                                                               #

def print_reg(filepath, nb, line, arg):
    print("\x1b[33m" + filepath + "\x1b[0m" + " :: line ->" + "[" + "\x1b[31m" 
            + str(nb) + "\x1b[0m" + "]" + " :: " + "\x1b[33m" + arg 
            + "\x1b[0m")
    print(line)

#==============================================================================#
#------------------------------------------------------------------------------#
# Regex Function                                                               #

def reg_multiple_assign(filepath, i, line):
    if i > 10 and bool(re.search(r'.*\s=\s.*\s=\s.*', line)) is True:
        print_reg(filepath, i, line, "MULTIPLE ASSIGNATION")

def reg_operator_ateof(filepath, i, line):
    if i > 12 and bool(re.search(r".*\*\/$", line)) is True:
        return
    if i > 12 and bool(re.search(r"(\_s[+*\-\/%*><&^~|=]$)", line)) is True \
    and bool(re.search(r"\.h>$", line)) is False:
        print_reg(filepath, i, line, "OPERATOR AT THE END OF LINE")
        return
    if i > 12 and bool(re.search(r"([+*\-\/%*><&^~|=]\s*$)", line)) is True \
    and bool(re.search(r"\.h>$", line)) is False:
        print_reg(filepath, i, line, "OPERATOR AT THE END OF LINE")
        return

#==============================================================================#
#------------------------------------------------------------------------------#
# Norme Checking on Includes

def parse_inc(filepath):
    if not os.path.isfile(filepath) or os.path.islink(filepath) \
            or os.stat(filepath).st_size == 0:
        return 0
    i = 1
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if i > 12 and bool(re.search(r'\[\]', line)) is True:
                print_reg(filepath, i, line, "VLA")
            if i > 12 and bool(re.search(r'\/\*', line)) is True:
                print_reg(filepath, i, line, "COMMENT IN HEADER")
            if i > 12 and bool(re.search(r'\.c\"', line)) is True:
                print_reg(filepath, i, line, "C FILE INCLUSION")
            reg_multiple_assign(filepath, i, line)
            line = next(file, None)
            i +=1
        line = next(file, None)
    return 0

#==============================================================================#
#------------------------------------------------------------------------------#
# Norme Checking on Sources

def parse_src(filepath):
    if not os.path.isfile(filepath) or os.path.islink(filepath) \
            or os.stat(filepath).st_size == 0:
        return 0
    i = 1
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if i > 12 and bool(re.search('\[\]', line)) is True:
                print_reg(filepath, i, line, "VLA")
            if i > 12 and bool(re.search('#[\t\s ]*define', line)) is True:
                print_reg(filepath, i, line, "DEFINE IN C FILE")
            reg_multiple_assign(filepath, i, line)
            reg_operator_ateof(filepath, i, line)
            line = next(file, None)
            i +=1
        line = next(file, None)
    return 0

#==============================================================================#
#------------------------------------------------------------------------------#
# Main of Norme                                                                #

def main_norme(srcs, incs):
    print("_______________________________________________________________________________")
    print(" --- NORME CHECKING "+"**/*.[ch]")
    print("")
    for i in range(len(incs)):
        parse_inc(incs[i])
        i += 1

    for i in range(len(srcs)):
        parse_src(srcs[i])
        i += 1
    return 0
