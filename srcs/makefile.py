#!/usr/bin/env python3

import re
import os
import sys
import fnmatch
import subprocess

# define COLOR_RED     "\x1b[31m"
# define COLOR_GREEN   "\x1b[32m"
# define COLOR_YELLOW  "\x1b[33m"
# define COLOR_BLUE    "\x1b[34m"
# define COLOR_MAGENTA "\x1b[35m"
# define COLOR_CYAN    "\x1b[36m"
# define COLOR_RESET   "\x1b[0m"

#==============================================================================#
#------------------------------------------------------------------------------#
# Utils                                                                        #

def find_files(path, pattern):
    r = []
    for root, dirs, files in os.walk(path):
        for file_ in files:
            filename = os.path.join(root, file_)
            if fnmatch.fnmatch(file_, pattern):
                r.append(filename)
    return r

#==============================================================================#
#------------------------------------------------------------------------------#
# Compile Checking                                                             #

def make_relink(path):
    valid = True
    print(" --- PLEASE WAIT DURING COMPILATION");
    print("")
    ret = subprocess.run(['make','all','-C' + str(path)], stdout = subprocess.PIPE)
    ret = subprocess.run(['make','all','-C' + str(path)], stdout = subprocess.PIPE)
    if bool(re.search(r'[.\n]*Nothing\sto\sbe\sdone\sfor\s.all.[.\n]*',
        ret.stdout.decode('utf-8'))) is False:
        print("\x1b[31m"+" --- ERROR RELINK"+"\x1b[0m")
        valid = False
    if ret.stdout.decode('utf-8').count('\n') > 3:
        print(ret.stdout.decode('utf-8'));
        print("\x1b[31m"+" --- ERROR RELINK"+"\x1b[0m")
        valid = False
    if valid == True:
        print("\x1b[32m"+" --- Ok :: Rule -> All"+"\x1b[0m")
    return valid 

def make_clean(path, m_bin):
    valid = True
    ob = []
    ret = subprocess.run(['make','clean','-C' + str(path)], stdout = subprocess.PIPE)
    ob = find_files(path, "*.o")
    if len(ob) > 0:
        print("\x1b[31m"+" --- ERROR CLEAN FAIL"+"\x1b[0m")
        valid = False
    ob = find_files(path, m_bin)
    if len(ob) < 1:
        print("\x1b[31m"+" --- ERROR CLEAN KILL BINARY"+"\x1b[0m")
        valid = False
    ret = subprocess.run(['make','all','-C' + str(path)], stdout = subprocess.PIPE)
    if valid == True:
        print("\x1b[32m"+" --- Ok :: Rule -> Clean"+"\x1b[0m")
    return valid

def make_fclean(path, m_bin):
    valid = True
    ob = []
    ret = subprocess.run(['make','fclean','-C' + str(path)], stdout = subprocess.PIPE)
    ob = find_files(path, "*.o")
    if len(ob) > 0:
        print("\x1b[31m"+" --- ERROR FCLEAN FAIL"+"\x1b[0m")
        valid = False
    ob = find_files(path, m_bin)
    if len(ob) == 1:
        print("\x1b[31m"+" --- ERROR FCLEAN DOESN'T KILL BINARY"+"\x1b[0m")
        valid = False
    if valid == True:
        print("\x1b[32m"+" --- Ok :: Rule -> Fclean"+"\x1b[0m")
    return valid

def make_re(path, m_bin):
    valid = True
    ob = []
    ret = subprocess.run(['make','re','-C' + str(path)], stdout = subprocess.PIPE)
    ob = find_files(path, "*.o")
    if len(ob) < 1:
        print("\x1b[31m"+" --- ERROR RE SHOULD COMPILE OBJECT"+"\x1b[0m")
        valid = False
    ob = find_files(path, m_bin)
    if len(ob) < 1:
        print("\x1b[31m"+" --- ERROR RE DOESN'T MAKE BINARY"+"\x1b[0m")
        valid = False
    if valid == True:
        print("\x1b[32m"+" --- Ok :: Rule -> Re"+"\x1b[0m")
    print("")
    return valid 

#==============================================================================#
#------------------------------------------------------------------------------#
# Norme Checking

def print_reg(filepath, nb, line):
    print("\x1b[33m" + filepath + "\x1b[0m" + " :: line ->" + "[" + "\x1b[31m" 
            + str(nb) + "\x1b[0m" + "]")
    print(line)

def makefile_rule(filepath):
    i = 0
    r_name = False
    r_clean = False
    r_fclean = False
    r_re = False
    r_all = False
    r_phony = False
    if not os.path.isfile(filepath) or os.path.islink(filepath) \
            or os.stat(filepath).st_size == 0:
        print(" --- Error :: Makefile is not a valid file")
        return 0
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if bool(re.search(r'^\$\(NAME\)', line)) is True:
                r_name = True
            if bool(re.search(r'^clean[\t ]*:', line)) is True:
                r_clean = True
            if bool(re.search(r'^fclean[\t ]*:', line)) is True:
                r_fclean = True
            if bool(re.search(r'^re[\t ]*:', line)) is True:
                r_re = True
            if bool(re.search(r'^all[\t ]*:', line)) is True:
                r_all = True
            if bool(re.search(r'^\.PHONY[\t ]*:', line)) is True:
                r_phony = True
            if r_all == True and bool(re.search(r'^all[\t ]*:.*\$(\(NAME\)|\{NAME\})', line)):
                r_call = True
            line = next(file, None)
            i +=1
        line = next(file, None)
        if r_name == False: print("The NAME rule is missing")
        if r_clean == False: print("The CLEAN rule is missing")
        if r_fclean == False: print("The FCLEAN rule is missing")
        if r_re == False: print("The RE rule is missing")
        if r_all == False: print("The ALL rule is missing")
        if r_phony == False: print("The PHONY rule is missing")
        if (r_name == False or r_clean == False or r_fclean == False
            or r_re == False or r_all == False or r_phony == False):
            print("")
            return False
    return True

def makefile_parse(filepath):
    valid = True
    i = 1
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if bool(re.search(r'-g', line)) is True:
                print_reg(filepath, i, line)
                valid = False
            if i > 12 and bool(re.search(r'\*[a-zA-Z\n\s]', line)) is True:
                print_reg(filepath, i, line)
                valid = False
            if bool(re.search(r'-[oO][23]', line)) is True:
                print_reg(filepath, i, line)
                valid = False
            line = next(file, None)
            i += 1
        line = next(file, None)
        return valid

def makefile_bin(filepath):
    s = ""
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if bool(re.search(r'.*NAME.*=[\t\s ]*([\D\d]*)', line)) is True:
                s = re.search(r'.*NAME.*=[\t\s ]*([\D\d]*)', line).group(1)
            line = next(file, None)
        line = next(file, None)
    if s == "":
        print(" --- ERROR PROGRAM NAME NOT FOUND")
    return s

#==============================================================================#
#------------------------------------------------------------------------------#
# Main of MAKEFILE                                                             #

def main_makefile(path):
    ret = 0
    filepath = ''
    m_bin = ''
    valid = True
    if os.path.exists(path + '/Makefile'):
        filepath = path + '/Makefile'
    elif os.path.exists(path + '/makefile'):
        filepath = path + '/makefile'
    else:
        print("\x1b[31m"+" --- ERROR No Makfile Found"+"\x1b[0m")
    print("_______________________________________________________________________________")
    print(" --- MAKE FILE CHECKING -> ",filepath)
    print("")
    valid = makefile_rule(filepath)
    if valid == True and makefile_parse(filepath) == False:
        valid = False
    m_bin = makefile_bin(filepath)
    m_bin = m_bin.strip()
    if valid == True:
        print("\x1b[32m"+" --- Ok :: Basic Checking"+"\x1b[0m")
    print("_______________________________________________________________________________")
    make_relink(path)
    make_clean(path, m_bin)
    make_fclean(path, m_bin)
    make_re(path, m_bin)
    return 0

