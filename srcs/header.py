#!/usr/bin/env python3

import re
import os
import sys

#==============================================================================#
#------------------------------------------------------------------------------#
# Utils function                                                               #

def print_reg(filepath, nb, line, arg):
    print("\x1b[33m" + filepath + "\x1b[0m" + " :: line ->" + "[" + "\x1b[31m" 
            + str(nb) + "\x1b[0m" + "]" + " :: " + "\x1b[33m" + arg 
            + "\x1b[0m")
    print(line)

#==============================================================================#
#------------------------------------------------------------------------------#
# Regex Function                                                               #

def reg_header(filepath, i, line):
    line = line.strip()
    h1 = "/* ************************************************************************** */"
    h2 = "/*                                                                            */"
    h3 = "/*                                                        :::      ::::::::   */"
    h4 = r"\/\*\s{3}[\D\d\s.]{51}:\+:\s{6}:\+:\s{4}:\+:\s{3}\*\/"
    h5 = "/*                                                    +:+ +:+         +:+     */"
    h6 = r'\/\*\s{3}By:\s[\D\s-]{9}<[\D\s]{8}@student.42.fr>\s{10}\+#\+\s\s\+:\+\s{7}\+#\+\s{8}\*/'
    h62 = r'\/\*\s{3}By:\s[\D\s-]{9}<marvin@42.fr>\s*\+#\+\s\s\+:\+\s{7}\+#\+\s{8}\*/'
    h7 = "/*                                                +#+#+#+#+#+   +#+           */"
    h8 = r"\/\*\s{3}Created:\s\d{4}\/\d{2}\/\d{2}\s(\d{2}[:\s]){3}by\s[\D\s-]{18}#\+#\s{4}#\+#\s{13}\*\/"
    h9 = r"\/\*\s{3}Updated:\s\d{4}\/\d{2}\/\d{2}\s(\d{2}[:\s]){3}by\s[\D\s-]{18}##\s{3}########.fr\s{7}\*\/"
    if i == 1 and not line == h1:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 2 and not line == h2:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 3 and not line == h3:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 4 and bool(re.search(h4, line)) is False:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 5 and not line == h5:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 6 and bool(re.search(h6, line)) is False:
        if i == 6 and bool(re.search(h62, line)) is False:
            print_reg(filepath, i, line, "HEADER IS NOT VALID :: Check your email please")
    if i == 7 and not line == h7:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 8 and bool(re.search(h8, line)) is False:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 9 and bool(re.search(h9, line)) is False:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 10 and not line == h2:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")
    if i == 11 and not line == h1:
        print_reg(filepath, i, line, "HEADER IS NOT VALID")

#==============================================================================#
#------------------------------------------------------------------------------#
# Norme Checking on Includes

def parse_inc_h(filepath):
    if not os.path.isfile(filepath) or os.path.islink(filepath) \
            or os.stat(filepath).st_size == 0:
        return 0
    i = 1
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if i < 12:
                reg_header(filepath, i, line)
            line = next(file, None)
            i +=1
        line = next(file, None)
    return 0

#==============================================================================#
#------------------------------------------------------------------------------#
# Norme Checking on Sources

def parse_src_h(filepath):
    if not os.path.isfile(filepath) or os.path.islink(filepath) \
            or os.stat(filepath).st_size == 0:
        return 0
    i = 1
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            if i < 12:
                reg_header(filepath, i, line)
            line = next(file, None)
            i +=1
        line = next(file, None)
    return 0

#==============================================================================#
#------------------------------------------------------------------------------#
# Main of Header Checking                                                      #

def main_header(srcs, incs):
    print("_______________________________________________________________________________")
    print(" --- HEADER CHECKING "+"**/*.[ch]")
    print("")
    for i in range(len(incs)):
        parse_inc_h(incs[i])
        i += 1

    for i in range(len(srcs)):
        parse_src_h(srcs[i])
        i += 1
    return 0
