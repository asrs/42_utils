#!/usr/bin/env python3
import os
import sys
import getopt
import fnmatch

from stat import *
from os import walk

sys.path.insert(0, './srcs/')
from makefile import *
from header import *
from malloc import *
from norme import *
from leak import *

#==============================================================================#
#------------------------------------------------------------------------------#
# Basic File check                                                             #

def warn_msg(str):
    print("\x1b[33m" + str + "\x1b[0m")

def check_path(path):
    if not os.path.exists(os.path.dirname(path)) or not os.path.isdir(path):
        print("FILE [" + str(path) +" ] is not a directory\n")
        ft_usage()

def check_makefile(path):
    if os.path.isfile(path + "/Makefile"):
        i = 1
        return True
    elif os.path.isfile(path + '/makefile'):
        i = 1
        return True
    else:
        warn_msg("")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg(" MSG :: no Makefile file Found")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg("")
    return False

def check_auteur(path):
    if os.path.exists(path + '/auteur') and not os.path.exists(path + '/author'):
        filepath = path + '/auteur'
    elif os.path.exists(path + '/author') and not os.path.exists(path + '/author'):
        filepath = path + '/author'
    else:
        warn_msg("")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg(" MSG :: no auteur or author file Found")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg("")
#        ASK FOR CONTINUE IF NO AUTHOR ?? 
        return
    if os.stat(path + '/auteur').st_size == 0 \
            and os.stat(path + '/author').st_size == 0:
        warn_msg("")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg(" MSG :: Empty Author File")
        warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
        warn_msg("")
        return
    ret = []
    with open(filepath, 'r') as file:
        line = next(file)
        while line:
            line = line.strip()
            chars = len(line)
            if chars > 8 or chars < 2:
                warn_msg("")
                warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
                warn_msg(" MSG :: auteur/author file login is not correct")
                warn_msg(" --- --- --- --- --- --- --- --- --- --- ")
                warn_msg("")
            ret.append(line);
            line = next(file, None)
        line = next(file, None)
    return ret

#==============================================================================#
#------------------------------------------------------------------------------#
# Launch Test Function                                                         #

def launch_makefile(g_yes, g_no):
    g_yes.append("\n")
    sys.stdout.write(" --- Would you like to check the Makefile ? [Y/n] : ")
    sys.stdout.flush()
    question = sys.stdin.readline()
    if question in g_yes:
        g_yes.remove("\n")
        return True
    elif question in g_no:
        g_yes.remove("\n")
        return False
    else:
        g_yes.remove("\n")
        launch_makefile(g_yes, g_no)

def launch_norm(g_yes, g_no):
    g_yes.append("\n")
    sys.stdout.write(" --- Would you like to check the Norm ? [Y/n] : ")
    sys.stdout.flush()
    question = sys.stdin.readline()
    if question in g_yes:
        g_yes.remove("\n")
        return True
    if question in g_no:
        g_yes.remove("\n")
        return False
    else:
        g_yes.remove("\n")
        launch_norm(g_yes, g_no)

def launch_header(g_yes, g_no):
    g_no.append("\n")
    sys.stdout.write(" --- Would you like to check the Header ? [y/N] : ")
    sys.stdout.flush()
    question = sys.stdin.readline()
    if question in g_yes:
        g_no.remove("\n")
        return True
    if question in g_no:
        g_no.remove("\n")
        return False
    else:
        g_no.remove("\n")
        launch_header(g_yes, g_no)

def launch_mallocs(g_yes, g_no):
    g_yes.append("\n")
    sys.stdout.write(" --- Would you like to check the Mallocs ? [Y/n] : ")
    sys.stdout.flush()
    question = sys.stdin.readline()
    if question in g_yes:
        g_yes.remove("\n")
        return True
    if question in g_no:
        g_yes.remove("\n")
        return False
    else:
        g_yes.remove("\n")
        launch_mallocs(g_yes, g_no)

def launch_leaks(g_yes, g_no):
    g_no.append("\n")
    sys.stdout.write(" --- Would you like to add helper for checking Leaks ? [y/N] : ")
    sys.stdout.flush()
    question = sys.stdin.readline()
    if question in g_yes:
        g_no.remove("\n")
        return True
    if question in g_no:
        g_no.remove("\n")
        return False
    else:
        g_no.remove("\n")
        launch_leaks(g_yes, g_no)

#==============================================================================#
#------------------------------------------------------------------------------#
# Get Directory Path for work                                                  #

def ft_getdir(args):
    i = len(args)
    if i < 1 or i > 2:
        ft_usage()
    return [args[i - 1]]

def ft_help():
    print("./Petunia.py [ -h -r ] .../path/to/project/directory/")
    print("")
    print(" -h :: Print help")
    print(" -c :: Remove Constructor && Malloc Define")
    print(" -r :: Remove Constructor && Malloc Define")
    print(" -a :: Launching all availabe checking")
    sys.exit(0)

def ft_usage():
    print("./Petunia.py [ -h -r -c -a ] .../path/to/project/directory/")
    sys.exit(0)

#==============================================================================#
#------------------------------------------------------------------------------#
# Function that return an array for include file and sources file

def find_files(path, pattern):
    r = []
    for root, dirs, files in os.walk(path):
        for file_ in files:
            filename = os.path.join(root, file_)
            if fnmatch.fnmatch(file_, pattern):
                r.append(filename)
    return r


#==============================================================================#
#------------------------------------------------------------------------------#
# Main of Petunia.py                                                           #

def main():
    g_clean = False
    g_all = False
    g_yes = ['y\n','Y\n','yes\n','Yes\n','YES\n']
    g_no = ['n\n','N\n','no\n','No\n','NO\n']

    try:
        opts, arg = getopt.getopt(sys.argv[1:],"hr")
    except getopt.GetoptError:
        ft_usage()
    for opt, arg in opts:
        if opt == '-h':
            ft_help()
        elif opt == '-r' or opt == '-c':
            g_clean = True
        elif opt == '-a':
            g_all = True

    input_dir = ft_getdir(sys.argv[1:])
    input_dir = input_dir[0]

    check_path(input_dir)

    in_make = check_makefile(input_dir)
    g_name = check_auteur(input_dir)

    print("")
    print("Starting the test in " + input_dir + " project directory.")
    if not g_name is None:
        print(g_name)
    print("")


    if g_clean == True:
        print("Ok, lets clean it up")
        sys.exit(0)

    g_srcs = find_files(input_dir, "*.c")
    g_incs = find_files(input_dir, "*.h")

    if in_make == True:
        choice_makefile = launch_makefile(g_yes, g_no)
    choice_norm = launch_norm(g_yes, g_no)
    choice_header = launch_header(g_yes, g_no)
    #choice_mallocs = launch_mallocs()
    #choice_leaks = launch_leaks()

    print("")
    print("")
    if (in_make == True and choice_makefile):
        main_makefile(input_dir)
    if (choice_norm):
        main_norme(g_srcs, g_incs)
    if (choice_header):
        main_header(g_srcs, g_incs)
    #if (choice_mallocs):
    #    main_malloc()
    #if (choice_leaks):
    #    main_leak()

if __name__ == "__main__":
    main()

###############################################################################
###############################################################################
